# Project base for Eleventy Sites with blog and i18n

Version of my [oscc-base template](https://gitlab.com/oscc-es/oscc-base) with translation ready setup (i18n).

## Additional features

- Translation ready
- Minimal modern favicon following the [Definitive edition of "How to Favicon in 2021"](https://dev.to/masakudamatsu/favicon-nightmare-how-to-maintain-sanity-3al7)
- sitemap.xml
- robots.txt
- Security HTTP headers
- 404 page
- Autoprefix and minify production css (with [Lightning CSS](https://lightningcss.dev/))
- [Automatically generate open graph images in Bernard Nijenhuis's way](https://bnijenhuis.nl/notes/2021-05-10-automatically-generate-open-graph-images-in-eleventy/)
- Minify production js
- Optimise size and image formats
- Image shorcode to wrap img with multi-source/sizes picture tags
- Service worker for PWA (Andy Bell's [Hylia](https://github.com/hankchizljaw/hylia) has been used as base)
- Offline page
- Fluid font-size (made with [Utopia](https://utopia.fyi/))

## Configuration

- Set default language on `src/_data/site.json` configuration: `lang: 'es'`
- Set all available languages on `src/_data/site.json` configuration: `"allLangs": ["es", "en"]`
- Set Languages files on every language folder as on `src/en/en.json`
- Translate strings on `src/_data/i18n/index.js`

## Getting started

- Run `npm install`
- Run `npm start` to run locally
- Run `npm run production` to do a prod build
- Run `npm run release` to generate/update the changelog file and increment the corresponding npm version on package.json
