---
title: 'Blog'
layout: 'layouts/feed.njk'
pagination:
  data: collections.blog
  size: 5
permalink: '/es/blog{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}{% endif %}/index.html'
paginationPrevText: 'Publicaciones posteriores'
paginationNextText: 'Publicaciones anteriores'
paginationAnchor: '#post-list'
---
