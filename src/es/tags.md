---
layout: 'layouts/feed.njk'
pagination:
  data: collections
  size: 1
  alias: tag
  filter:
    - all
    - post
    - posts
    - tagList
    - blog
  addAllPagesToCollections: true
permalink: '/es/tag/{{ tag | slugify }}/'
eleventyComputed:
  title: Publicaciones del blog archivadas como “{{ tag }}”
---
