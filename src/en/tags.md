---
layout: 'layouts/feed.njk'
pagination:
  data: collections
  size: 1
  alias: tag
  filter:
    - all
    - post
    - posts
    - tagList
    - blog
  addAllPagesToCollections: true
permalink: '/en/tag/{{ tag | slugify }}/'
eleventyComputed:
  title: Blog posts filed under “{{ tag }}”
---
