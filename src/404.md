---
title: '404: No encontrado'
layout: layouts/page.njk
permalink: /404.html
eleventyExcludeFromCollections: true
---

Lo sentimos, pero ese contenido no se puede encontrar. Por favor, [vuelva a la página de inicio](/es/).
